v2.2.4
- updated: translations, many thanks to all translators!

v2.2.3
- updated: translations, many thanks to all translators!

v2.2.2
- fixed: contributor string not being fully translatable
- updated: translations, many thanks to all translators!

v2.2.1
- improved: positioning of slider labels slightly better
- updated: translations, many thanks to all translators!

v.2.2.0
- add: all RGB color values to slider labels
- add: option to sync all three sliders in main screen

v2.1.6
- fixed: typo in about page
- updated: translations, many thanks to all translators!

v2.1.5
- updated: translations and added several languages, many thanks to all translators!

v2.1.4
- improved: use UBports Hosted Weblate for translation services, thanks to Weblate for providing us with a free service
- improved: some spelling, thanks to @kingu
- updated: translations, many thanks to all translators!

v2.1.3
- add: focal support

v2.1.2
- update: Dutch and French translations, thanks @Vistaus and @Anne17

v2.1.1
- fix: visibility issues in landscape

v2.1.0
- add: button to quickly swap text and background color
- add: check to avoid possible low contrast between text and background
- add: color initials to sliders
- fix: set initial slider colors

v2.0.0.
- add: migrate the app to Gitlab
- add: new maintainer Daniel Danfro Frost
- add: allow copy of hex code
- add: about page
- add: German translation
- fix: made the app arch all instead of being architecture dependend by removing c++ code
- fix: settings behavior
- fix: use UT style header and icons
